Feidhm Soghluaiseacht
=====================

Mobile Application
------------------

*fs* is a set of tools that I use to build mobile web applications,
specifically for [Anrdoid][android] and [iPhone][iphone] devices (though I
like them to work in [FireFox][firefox] and [Chrome][chrome], too.

Included are some HTML5 templates, JavaScripts, and CSS stylesheets. Have a
look at the [home page][home] for some examples.

  [home]: http://jjbuckley.github.com/fs "FS Homepage"
  [android]: http://android.com "Android"
  [iphone]: http://apple.com/iphone "Apple iPhone"
  [firefox]: http://mozilla.org/firefox "FireFox"
  [chrome]: http://google.com/chrome
